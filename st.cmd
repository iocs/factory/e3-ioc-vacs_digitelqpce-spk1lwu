require essioc
require vac_ctrl_digitelqpce,1.5.0

iocshLoad("${essioc_DIR}/common_config.iocsh")

epicsEnvSet("DEVICE_NAME", "Spk-010LWU:Vac-VEPI-10001")
epicsEnvSet("MOXA_HOSTNAME", "moxa-vac-dtl-30-u005.tn.esss.lu.se")
epicsEnvSet("MOXA_PORT", "4001")

iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_ctrl_digitelqpce_moxa.iocsh", "DEVICENAME = $(DEVICE_NAME), IPADDR = $(MOXA_HOSTNAME), PORT = $(MOXA_PORT)")

iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = Spk-010LWU:Vac-VPN-10000, CHANNEL = 1, CONTROLLERNAME = $(DEVICE_NAME)")
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = Spk-010LWU:Vac-VPN-20000, CHANNEL = 2, CONTROLLERNAME = $(DEVICE_NAME)")
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = Spk-010LWU:Vac-VPN-30000, CHANNEL = 3, CONTROLLERNAME = $(DEVICE_NAME)")
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = Spk-010LWU:Vac-VPN-40000, CHANNEL = 4, CONTROLLERNAME = $(DEVICE_NAME)")
